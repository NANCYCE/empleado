
package pruebaempleadoporcomision;

/**
 *
 * @author Nancy Jimenez
 */
public class PruebaEmpleadoPorComision {

    public static void main(String[] args) {
    
      EmpleadoPorComision empleado = new EmpleadoPorComision("Nancy", "Luis", "444-44-4444", 10000, .06);
        System.out.println(
                "Informacion del empleado obtenida por los metodos get:");
        System.out.printf("%n%s %s%n", "El primer nombre es",
                empleado.getClass());
        System.out.printf("%s %s%n", "El apellido paterno es",
                empleado.getClass());
        System.out.printf("%s %s%n", "El numero del seguro social es",
                empleado.getClass());
        System.out.printf("%s %.2f%n", "Las ventas brutas son",
                empleado.getClass());
        System.out.printf("%s %.2f%n", "La tarifa de comision es",
                empleado.getClass());
        
        empleado.setVentasBrutas(500);
        empleado.setTarifaComision(.1);
        
        System.out.printf("%n%s:%n%n%s%n",
                "Informacion actualizada del empleado, obtenida mediante toString",
                empleado);
    }
}
